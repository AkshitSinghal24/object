function pairs(obj) {
    let arr = []
    for(let key in obj){
        arr.push([key,obj[key]]);
    }
    return arr;
    // Convert an object into a list of [key, value] pairs.
    // http://underscorejs.org/#pairs
}
module.exports = pairs;