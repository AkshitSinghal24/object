function values(obj) {
    let arr = [];
    for(let key in obj){
        arr.push(obj[key]);
    }
    return arr;
    // Return all of the values of the object's own properties.
    // Ignore functions
    // http://underscorejs.org/#values
}
module.exports = values;