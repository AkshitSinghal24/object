function mapObject(obj, cb) {
    let ans = cb(obj);
    return ans;
    // Like map for arrays, but for objects. Transform the value of each property in turn by passing it to the callback function.
    // http://underscorejs.org/#mapObject
}
module.exports = mapObject;