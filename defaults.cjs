function defaults(obj, defaultProps) {
    for(let keys in defaultProps){
        if(!obj[keys]){
            obj[keys] = defaultProps[keys];
        }
    }
    return obj;
    // Fill in undefined properties that match properties on the `defaultProps` parameter object.
    // Return `obj`.
    // http://underscorejs.org/#defaults
}

module.exports = defaults;